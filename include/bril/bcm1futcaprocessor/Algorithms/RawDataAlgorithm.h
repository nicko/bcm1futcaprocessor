#ifndef _bril_bcm1f_utcaprocessor_rawdata_h_
#define _bril_bcm1f_utcaprocessor_rawdata_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"

namespace bril {

    namespace bcm1futcaprocessor {

        class RawDataAnalyzer : public RawDataAlgorithm
        {

          public:
            RawDataAnalyzer (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory*, toolbox::mem::Pool* )
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;
                m_nentries = std::stoi( props.getProperty( "nentries" ) );
                m_bins = std::stoi( props.getProperty( "binning" ) );
                m_mip_spectrum.resize( m_bins );
                m_tp_spectrum.resize( m_bins );
            }

            virtual void compute (  bril::bcm1futcaprocessor::RawHistogramVector& histovec, const Beam&, chart_data_t& mip_peak, chart_data_t& tp, MonitoringVariables* monitoring )
            {
                for ( auto& histobuffer : histovec)
                {
                    for ( auto& hist : histobuffer.m_channelMap )
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        auto& mip_values = m_mip_values[hist.first];
                        auto& mip_spectrum = m_mip_spectrum[hist.first];
                        for ( float mip : find_mips( hist.second ) ) //update histogram and timeseries at once
                        {
                            mip_values.push_back( mip );
                            ++mip_spectrum[mip_bin( mip )];
                            if ( mip_values.size > m_nentries )
                            {
                                --mip_spectrum[mip_bin( mip_values.front() )];
                                mip_values.pop_front();
                            }
                        }
                        auto& tp_values = m_tp_values[hist.first];
                        auto& tp_spectrum = m_tp_spectrum[hist.first];
                        float tp = find_tp( hist.second );
                        tp_values.push_back( tp ); //update histogram and timeseries at once
                        ++tp_spectrum[tp_bin( tp )];
                        if ( tp_values.size > m_nentries )
                        {
                            --tp_spectrum[tp_bin( tp_values.front() )];
                            tp_values.pop_front();
                        }
                    }
                }
            }

          private:
            float find_tp( const Histogram& );

            std::vector<float> find_mips( const Histogram& );

            size_t mip_bin( float );

            size_t tp_bin( float );

          private:
            ChannelInfo* m_channelInfo;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
            size_t m_nentries;
            size_t m_bins;
            std::map<HistogramIdentifier, std::deque<float>> m_mip_values;
            std::map<HistogramIdentifier, std::deque<float>> m_tp_values;
            std::map<HistogramIdentifier, std::vector<int>> m_mip_spectrum;
            std::map<HistogramIdentifier, std::vector<int>> m_tp_spectrum;

        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
