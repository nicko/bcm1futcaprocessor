# $Id$
# I'm a bcm1f bril utca processor
#
BUILD_HOME:=$(shell pwd)/../../..

include $(XDAQ_ROOT)/config/mfAutoconf.rules
include $(XDAQ_ROOT)/config/mfDefs.$(XDAQ_OS)
include $(XDAQ_ROOT)/config/mfDefs.extern_coretools
include $(XDAQ_ROOT)/config/mfDefs.coretools
include $(XDAQ_ROOT)/config/mfDefs.extern_powerpack
include $(XDAQ_ROOT)/config/mfDefs.powerpack
include $(XDAQ_ROOT)/config/mfDefs.bril_worksuite

Project=daq
Package=bril/bcm1futcaprocessor

Sources = $(wildcard src/common/*.cc)
IncludeDirs = \
	$(XI2O_UTILS_INCLUDE_PREFIX) \
        $(XERCES_INCLUDE_PREFIX) \
        $(LOG4CPLUS_INCLUDE_PREFIX) \
        $(CGICC_INCLUDE_PREFIX) \
        $(XCEPT_INCLUDE_PREFIX) \
        $(CONFIG_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX) \
        $(PT_INCLUDE_PREFIX) \
	$(XDAQ_INCLUDE_PREFIX) \
        $(XDATA_INCLUDE_PREFIX) \
        $(XOAP_INCLUDE_PREFIX) \
        $(XGI_INCLUDE_PREFIX) \
        $(I2O_INCLUDE_PREFIX) \
        $(XI2O_INCLUDE_PREFIX) \
        $(B2IN_NUB_INCLUDE_PREFIX) \
	$(INTERFACE_BRIL_INCLUDE_PREFIX) 

LibraryDirs = 
UserSourcePath =
UserCFlags = 
UserCCFlags = -std=c++11 -D__CONSOLE__
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags = 
DependentLibraryDirs = 
DependentLibraries = 
ExternalObjects = 
DynamicLibrary=brilbcm1futcaprocessor
StaticLibrary=
TestLibraries=
TestExecutables=
include $(XDAQ_ROOT)/config/Makefile.rules
include $(XDAQ_ROOT)/config/mfRPM.rules
include webdev.rules
